let imgCotxe;
let imgPoli;
let yPos = 0;
let randomNumber;
let gameOver = false;
let points = 0;
let velocity = 0.1;

function preload() {
  imgCotxe = loadImage('ferrari.png')
  imgPoli = loadImage('poli.png')
}

function setup() {
  const cv = createCanvas(800, 1390)
  cv.parent("canvas")
  background(255, 5, 50)
  randomNumber = Math.round(Math.random()) * 420 + 80;
  draw()
}

function checkCollision(x1, y1, w1, h1, x2, y2, w2, h2) {
    // funció per detectar col·lisions entre dos objectes
    // rep les coordenades i mides dels dos objectes
    return x1 + w1 >= x2 && x1 <= x2 + w2 && y1 + h1 >= y2 && y1 <= y2 + h2;
}

function reiniciar() {
    location.reload();
}
  

function endGame(){
    textSize(100);
    text("GAME OVER", (width/2)-300, (height/2) - 100);

    button = createButton('Restart');
    button.size(200, 100);
    button.position(width/2 - button.width/2, height/2 - button.height/2);
    button.mousePressed(reiniciar);
}


function draw() {

  //fons gris
  background(85, 85, 85)

  //linea discontinua
  stroke(255)
  strokeWeight(20);
  drawingContext.setLineDash([50, 50]);
  line(390, 20, 390, height)
  drawingContext.setLineDash([]);

  //contador punts
  textSize(32);
  text(points, 100, 100);

  //cotxe
  let carWidth = imgCotxe.width / 5;
  let carHeight = imgCotxe.height / 5;
  let carX = mouseX;
  let carY = height / 1.5;
  image(imgCotxe, carX, carY, carWidth, carHeight);

  //poli
  let policeWidth = imgPoli.width / 1.5;
  let policeHeight = imgPoli.height / 1.5;
  let policeX = randomNumber;
  let policeY = yPos;
  image(imgPoli, policeX, policeY, policeWidth, policeHeight);

  // detect collision
  if (checkCollision(carX, carY, carWidth, carHeight, policeX, policeY, policeWidth, policeHeight)) {
    gameOver = true;
  }

  //animació moure cotxe
  if (!gameOver) {
    // Augmenta la posició vertical de la policia per fer que es mogui cap avall
    yPos += velocity;

    // Si la policia arriba al final de la pantalla, reinicia la seva posició vertical i x
    if (yPos > height) {
      yPos = -500;
      points += 1
      velocity += 0.05;
      randomNumber = Math.round(Math.random()) * 420 + 80;
    }

    requestAnimationFrame(draw)
  }else{
    endGame()
  }
}